(ns sibyl)

(defprotocol HasPolicy
  (policy [x]))

(defprotocol Condition
  (evaluate [this subject object]))

(defrecord Policy [disabling-rules enabling-rules])

(defrecord Sibyl [store])

(defn value [store p subject name object]
  (loop [policy p]
    (if-let [c (get (:disabling-rules policy) name)]
      (let [[failed msg] (evaluate c subject object)]
        (if failed
          {:enabled false, :message msg}
          (recur (update-in policy [:disabling-rules] dissoc name))))
      (if-let [c (get (:enabling-rules policy) name)]
        (let [[passed val] (evaluate c subject object)]
          (if passed
            {:enabled true, :value val}
            {:enabled false, :message val}))
        {:enabled false, :message :not-defined}))))

(defn query [sibyl subject predicate object]
  (when-let [p (policy object)]
    (value (:store sibyl) p subject predicate object)))

(def empty-policy (Policy. {} {}))

(defrecord FnCondition [f]
  Condition
  (evaluate [this subject object] (f subject object)))

(defn iff [f x passed failed]
  (let [ok? (f x)]
    [ok? (if ok? passed failed)]))

(defn subject-condition [passed failed f]
  (FnCondition. (fn [s o]
                    (iff f s passed failed))))

(defn object-condition [passed failed f]
  (FnCondition. (fn [s o]
                    (iff f o passed failed))))

(defn or-condition? [conditions]
  (FnCondition. (fn [s o]
    (reduce (fn [[_ failures] c]
              (let [r (evaluate c s o)]
                (if (first r)
                      (reduced r)
                      (update-in r [1] (partial conj failures)))))
            [ false [] ]
            conditions))))

(defn and-condition? [conditions]
  (FnCondition. (fn [s o]
    (reduce (fn [r c]
              (let [rr (evaluate c s o)]
                (if (first rr)
                    (update-in r [1] conj (rr 1))
                    (reduced rr))))
            [ true [] ]
            conditions))))

(defn not-cond [c]
  (FnCondition. (fn [s o]
                  (update-in (evaluate c s o) [0] not))))
(defn none-of [conditions]
  (and-condition? (map not-cond conditions)))

