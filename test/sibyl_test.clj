(ns sibyl_test
  (:use clojure.test sibyl))

(def s-even? (subject-condition :even-subject :odd-subject even?))
(def o-even? (object-condition :even-object :odd-object even?))
(def s-gt-10 (subject-condition :subj-gt-10 :subj-lt-10 #(< 10 %)))
(def s-mul-3 (subject-condition :subj-mul-3 :subj-not-mul-3 #(= 0 (mod % 3))))
(def conds [s-even? s-gt-10 s-mul-3])

(testing "conditions"
  (let [or-c (or-condition? conds)
        and-c (and-condition? conds)
        not-even (not-cond s-even?)
        none-c (none-of conds)]
    (is (= [true :subj-gt-10] (evaluate or-c 11 nil)))
    (is (= [true :even-subject](evaluate or-c 10 nil)))
    (is (= [true :subj-mul-3] (evaluate or-c 9 nil)))
    (is (= [false [:odd-subject :subj-lt-10 :subj-not-mul-3]] (evaluate or-c 7 nil)))
    (is (= [true [:even-subject :subj-gt-10 :subj-mul-3]] (evaluate and-c 12 nil)))
    (is (= [true [:odd-subject :subj-lt-10 :subj-not-mul-3]] (evaluate none-c 7 nil)))
    (is (= [false :even-subject] (evaluate none-c 8 nil)))
    (is (= [false :even-subject] (evaluate not-even 10 nil)))
    (is (= [true :odd-subject] (evaluate not-even 9 nil)))))

(def basic-policy
  (-> empty-policy
      (assoc-in [:disabling-rules :ok] o-even?)
      (assoc-in [:enabling-rules :ok] s-even?)))

(def combined-policy
  (-> empty-policy
      (assoc-in [:disabling-rules :ok] (and-condition? conds))
      (assoc-in [:enabling-rules :ok] (or-condition? conds))))

(defn stateful-policy [state]
  (letfn [(statefully [k f] (fn [x] (swap! state update-in [k] inc) (f x)))]
    (sibyl.Policy.
      {:ok (object-condition :even-object :odd-object (statefully :disabled? even?))}
      {:ok (subject-condition :even-subject :odd-subject (statefully :enabled? even?))})))

(testing "value"
  ;; Even subject, but odd object
  (is (= {:enabled false, :message :even-object}
         (value nil basic-policy 1 :ok 2)))
  (is (= {:enabled false, :message :odd-subject}
         (value nil basic-policy 1 :ok 1)))
  (is (= {:enabled true, :value :even-subject}
         (value nil basic-policy 2 :ok 1)))
  (is (= {:enabled false, :message :not-defined}
         (value nil basic-policy 2 :foo 1)))
  ;; One of, but not all:
  (is (= {:enabled true, :value :even-subject} (value nil combined-policy 8 :ok nil)))
  (is (= {:enabled true, :value :subj-mul-3} (value nil combined-policy 9 :ok nil)))
  (is (= {:enabled true, :value :even-subject} (value nil combined-policy 10 :ok nil)))
  (is (= {:enabled true, :value :subj-gt-10} (value nil combined-policy 11 :ok nil)))
  (is (= {:enabled false, :message [:even-subject :subj-gt-10 :subj-mul-3]}
         (value nil combined-policy 12 :ok nil)))
  (is (= {:enabled false, :message [:odd-subject :subj-lt-10 :subj-not-mul-3]}
         (value nil combined-policy 7 :ok nil)))
  )
